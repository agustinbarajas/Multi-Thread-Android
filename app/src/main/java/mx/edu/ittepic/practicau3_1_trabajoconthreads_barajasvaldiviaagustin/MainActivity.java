package mx.edu.ittepic.practicau3_1_trabajoconthreads_barajasvaldiviaagustin;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView puntosJ1, puntosJ2, puntosJ3, puntosJ4;
    TextView totalJ1, totalJ2, totalJ3, totalJ4;
    TextView turno, ganador;
    ImageView dado1, dado2;
    Button partida;
    Thread jugador1, jugador2, jugador3, jugador4;
    int turn, pointsJ1, pointsJ2, pointsJ3, pointsJ4;
    String playerwinner;
    boolean[] iniciados = {false, false, false, false};

    int[] caras = {R.drawable.cara1, R.drawable.cara2, R.drawable.cara3,
                    R.drawable.cara4, R.drawable.cara5, R.drawable.cara6};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        puntosJ1 = findViewById(R.id.puntosJ1);
        puntosJ2 = findViewById(R.id.puntosJ2);
        puntosJ3 = findViewById(R.id.puntosJ3);
        puntosJ4 = findViewById(R.id.puntosJ4);
        totalJ1 = findViewById(R.id.totalJ1);
        totalJ2 = findViewById(R.id.totalJ2);
        totalJ3 = findViewById(R.id.totalJ3);
        totalJ4 = findViewById(R.id.totalJ4);
        turno = findViewById(R.id.turno);
        ganador = findViewById(R.id.ganador);
        dado1 = findViewById(R.id.dado1);
        dado2 = findViewById(R.id.dado2);
        partida = findViewById(R.id.partida);
        turn = 1;
        playerwinner = "";
        pointsJ1 = pointsJ2 = pointsJ3 = pointsJ4 = 0;

        partida.setOnClickListener(new View.OnClickListener(){
           @Override
            public void onClick(View v) {
               iniciar();
            }
        });
    }
    /*
    public void run() {
        int c1 = aleatorio();
        int c2 = aleatorio();
        dado1.setImageResource(caras[c1]);
        dado2.setImageResource(caras[c2]);
        puntosJ1.setText(c1 + c2 + 2 + "");
        pointsJ1 += c1 + c2 + 2;
        totalJ1.setText(pointsJ1 + "");
        turno.setText(turn + "");
    }
    * */

    private void iniciar() {
            jugador1 = new Thread() {
                public void run() {
                    try {
                        while (turn < 4) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    System.out.println("jugador 1 en ejecución");
                                    int c1 = aleatorio();
                                    int c2 = aleatorio();
                                    dado1.setImageResource(caras[c1]);
                                    dado2.setImageResource(caras[c2]);
                                    puntosJ1.setText(c1 + c2 + 2 + "");
                                    pointsJ1 += c1 + c2 + 2;
                                    totalJ1.setText(pointsJ1 + "");
                                    turno.setText(turn + "");
                                }
                            });
                            sleep(1000);
                            if (jugador2.getState() == State.NEW && !iniciados[1]) {
                                System.out.println("jugador 2 iniciado");
                                jugador2.start();
                                iniciados[1] = true;
                            }
                            synchronized (jugador2) {
                                if (jugador2.getState() == State.WAITING) {
                                    System.out.println("jugador 2 reiniciando");
                                    jugador2.notify();
                                    System.out.println("jugador 2 " + jugador2.getState());
                                }
                            }
                            synchronized (jugador1) {
                                System.out.println("jugador 1 parado");
                                System.out.println("jugador 2 " + jugador2.getState());
                                jugador1.wait();
                            }
                        }
                    } catch (InterruptedException e) {

                    }
                }
            };
            jugador2 = new Thread() {
                public void run() {
                    try {
                        while (turn < 4) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    System.out.println("jugador 2 en ejecución");
                                    int c1 = aleatorio();
                                    int c2 = aleatorio();
                                    dado1.setImageResource(caras[c1]);
                                    dado2.setImageResource(caras[c2]);
                                    puntosJ2.setText(c1 + c2 + 2 + "");
                                    pointsJ2 += c1 + c2 + 2;
                                    totalJ2.setText(pointsJ2 + "");
                                    turno.setText(turn + "");
                                }
                            });
                            sleep(1000);
                            if (jugador3.getState() == State.NEW && !iniciados[2]) {
                                System.out.println("jugador 3 iniciado");
                                jugador3.start();
                                iniciados[2] = true;
                            }
                            synchronized (jugador3) {
                                if (jugador3.getState() == State.WAITING) {
                                    System.out.println("jugador 3 reiniciando");
                                    jugador3.notify();
                                    System.out.println("jugador 3 " + jugador3.getState());
                                }
                            }
                            synchronized (jugador2) {
                                System.out.println("jugador 2 parado");
                                System.out.println("jugador 3 " + jugador3.getState());
                                jugador2.wait();
                            }
                        }
                    } catch (InterruptedException e) {

                    }
                }
            };
            jugador3 = new Thread() {
                public void run() {
                    try {
                        while (turn < 4) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    System.out.println("jugador 3 en ejecución");
                                    int c1 = aleatorio();
                                    int c2 = aleatorio();
                                    dado1.setImageResource(caras[c1]);
                                    dado2.setImageResource(caras[c2]);
                                    puntosJ3.setText(c1 + c2 + 2 + "");
                                    pointsJ3 += c1 + c2 + 2;
                                    totalJ3.setText(pointsJ3 + "");
                                    turno.setText(turn + "");
                                }
                            });
                            sleep(1000);
                            if (jugador4.getState() == State.NEW && !iniciados[3]) {
                                System.out.println("jugador 4 iniciado");
                                jugador4.start();
                                iniciados[3] = true;
                            }
                            synchronized (jugador4) {
                                if (jugador4.getState() == State.WAITING) {
                                    System.out.println("jugador 4 reiniciando");
                                    jugador4.notify();
                                    System.out.println("jugador 4 " + jugador4.getState());
                                }
                            }
                            synchronized (jugador3) {
                                System.out.println("jugador 3 parado");
                                System.out.println("jugador 4 " + jugador4.getState());
                                jugador3.wait();
                            }
                        }
                    } catch (InterruptedException e) {

                    }
                }
            };
            jugador4 = new Thread() {
                public void run() {
                    try {
                        while (turn < 4) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    System.out.println("jugador 4 en ejecución");
                                    int c1 = aleatorio();
                                    int c2 = aleatorio();
                                    dado1.setImageResource(caras[c1]);
                                    dado2.setImageResource(caras[c2]);
                                    puntosJ4.setText(c1 + c2 + 2 + "");
                                    pointsJ4 += c1 + c2 + 2;
                                    totalJ4.setText(pointsJ4 + "");
                                    turno.setText(turn + "");
                                    if (turn == 4){
                                        ganador.setText(winner(new int[]{pointsJ1, pointsJ2, pointsJ3, pointsJ4}));
                                    }
                                }
                            });
                            turn++;
                            sleep(1000);
                            if (jugador1.getState() == State.NEW && !iniciados[0]) {
                                System.out.println("jugador 1 iniciado");
                                jugador1.start();
                            }
                            synchronized (jugador1) {
                                if (jugador1.getState() == State.WAITING) {
                                    System.out.println("jugador 1 reiniciando");
                                    jugador1.notify();
                                    System.out.println("jugador 1 " + jugador1.getState());
                                }
                            }
                            synchronized (jugador4) {
                                System.out.println("jugador 1 reiniciado "+jugador1.getState());
                                System.out.println("jugador 4 parado");
                                jugador4.wait();
                            }
                        }
                    } catch (InterruptedException e) {

                    }
                }
            };
        jugador1.start();
        iniciados[0] = true;
    }

    public boolean onCreateOptionsMenu(Menu m){
        getMenuInflater().inflate(R.menu.menu, m);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem mi){
        switch (mi.getItemId()){
            case R.id.nuevapartida:
                puntosJ1.setText("");
                puntosJ2.setText("");
                puntosJ3.setText("");
                puntosJ4.setText("");
                totalJ1.setText("");
                totalJ2.setText("");
                totalJ3.setText("");
                totalJ4.setText("");
                ganador.setText("");
                turno.setText("");
                turn = 1;
                pointsJ1 = pointsJ2 = pointsJ3 = pointsJ4 = 0;
                iniciados[0] = iniciados[1] = iniciados[2] = iniciados[3] =  false;
                playerwinner = "";
                dado1.setImageResource(R.drawable.cara1);
                dado2.setImageResource(R.drawable.cara1);
                Toast.makeText(MainActivity.this, "Nueva partida iniciada", Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }

    public int aleatorio(){
        int numAleatorio=6;
        while(numAleatorio > 5){
            numAleatorio = (int)(Math.random()*10);
        }
        return numAleatorio;
    }
    public String winner(int[] playerpoints){
        String player = "";
        int points = 0;
        for (int point = 0; point < playerpoints.length; point++){
            if (playerpoints[point] == points){
                player = "Hay múltiples ganadores";
                continue;
            }
            if (playerpoints[point] > points){
                player = "Jugador " + (point + 1);
                points = playerpoints[point];
            }
        }
        return player;
    }
}
